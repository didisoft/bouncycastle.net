﻿#if NETFX_CORE
using System;

namespace System.IO
{
    public static class DidiSoftExtensions
    {
        public static void Close(this System.IO.Stream str)
        {
            str.Flush();
            str.Dispose();
        }

        public static void Close(this System.IO.TextWriter str)
        {
            str.Flush();
            str.Dispose();
        }        
    }
}
#endif