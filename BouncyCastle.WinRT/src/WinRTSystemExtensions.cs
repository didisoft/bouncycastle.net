﻿#if NETFX_CORE
using System;
using System.Collections.Generic;
using System.Reflection;

namespace System
{
    public static class Console
    {
        public static void WriteLine(string s)
        {
        }
        public static void WriteLine(object s)
        {
        }
        public static void WriteLine()
        {
        }
    }

    public static class TypeExtensions
    {
        public static bool IsInstanceOfType(this System.Type tip, Object obj)
        {            
            return obj != null && tip.IsAssignableFrom(obj.GetType());
        }

        internal static bool ImplementInterface(this Type type, Type ifaceType)
        {
            while (type != null)
            {
                IEnumerator<Type> enumerator = type.GetTypeInfo().ImplementedInterfaces.GetEnumerator();
                while (enumerator.MoveNext())
                {
                    Type i = enumerator.Current;
                    if (i == ifaceType || (i != null && i.ImplementInterface(ifaceType)))
                    {
                        return true;
                    }
                }
                type = type.GetTypeInfo().BaseType;
            }
            return false;
        }

        public static bool IsAssignableFrom(this Type type, Type c)
        {
            if (c == null)
            {
                return false;
            }
            if (type == c)
            {
                return true;
            }

            if (c.GetTypeInfo().IsSubclassOf(c))
            {
                return true;
            }

            if (type.GetTypeInfo().IsInterface)
            {
                return c.ImplementInterface(type);
            }

            if (type.IsGenericParameter)
            {
                Type[] genericParameterConstraints = type.GetTypeInfo().GetGenericParameterConstraints();
                for (int i = 0; i < genericParameterConstraints.Length; i++)
                {
                    if (!genericParameterConstraints[i].IsAssignableFrom(c))
                    {
                        return false;
                    }
                }
                return true;
            }
            return false;
        }
    }

    public static class StringExtensions
    {
        public static string ToUpper(this string s, System.Globalization.CultureInfo c) 
        {
            return s.ToUpperInvariant();
        }
        public static string ToLower(this string s, System.Globalization.CultureInfo c)
        {
            return s.ToLowerInvariant();
        }
    }
}
#endif