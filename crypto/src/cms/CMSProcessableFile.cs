using System;
using System.IO;

using Org.BouncyCastle.Utilities.IO;

#if NETFX_CORE
using Windows.Storage;
using Windows.Foundation;
using System.Threading.Tasks;
#endif

namespace Org.BouncyCastle.Cms
{
    /**
    * a holding class for a file of data to be processed.
    */
    public class CmsProcessableFile
        : CmsProcessable, CmsReadable
    {
        private const int DefaultBufSize = 32 * 1024;

#if !NETFX_CORE
		private readonly FileInfo	_file;
#else
        private readonly StorageFile _file;
#endif
        private readonly int _bufSize;

#if !NETFX_CORE
		public CmsProcessableFile(FileInfo file)
#else
        public CmsProcessableFile(StorageFile file)
#endif
            : this(file, DefaultBufSize)
        {
        }

#if !NETFX_CORE
		public CmsProcessableFile(FileInfo file, int bufSize)
#else
        public CmsProcessableFile(StorageFile file, int bufSize)
#endif
        {
            _file = file;
            _bufSize = bufSize;
        }

        public virtual Stream GetInputStream()
        {
#if !NETFX_CORE
            return new FileStream(
                _file.FullName, FileMode.Open, FileAccess.Read, FileShare.Read, _bufSize);
#else
            Task<Stream> t = _file.OpenStreamForReadAsync();

            t.Wait();
            return t.Result;
#endif
        }

        public virtual void Write(Stream zOut)
        {
            using (Stream inStr = GetInputStream())
            {
                Streams.PipeAll(inStr, zOut);
            }
        }

        /// <returns>The file handle</returns>
        public virtual object GetContent()
        {
            return _file;
        }
    }
}
