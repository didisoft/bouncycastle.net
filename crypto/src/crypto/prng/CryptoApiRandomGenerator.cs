#if !NETCF_1_0

using System;

#if NETFX_CORE
using Windows.Security.Cryptography;
using Windows.Storage.Streams;
#else
using System.Security.Cryptography;
#endif

namespace Org.BouncyCastle.Crypto.Prng
{
    /// <summary>
    /// Uses Microsoft's RNGCryptoServiceProvider
    /// </summary>
    public class CryptoApiRandomGenerator
        : IRandomGenerator
    {
#if !NETFX_CORE
		private readonly RNGCryptoServiceProvider rndProv;
#endif
        public CryptoApiRandomGenerator()
        {
#if !NETFX_CORE
            rndProv = new RNGCryptoServiceProvider();
#endif
        }

        #region IRandomGenerator Members

        public virtual void AddSeedMaterial(byte[] seed)
        {
            // We don't care about the seed
        }

        public virtual void AddSeedMaterial(long seed)
        {
            // We don't care about the seed
        }

        public virtual void NextBytes(byte[] bytes)
        {
#if !NETFX_CORE
			rndProv.GetBytes(bytes);
#else
            IBuffer buf = CryptographicBuffer.GenerateRandom((uint)bytes.Length);
            CryptographicBuffer.CopyToByteArray(buf, out bytes);
#endif
        }

        public virtual void NextBytes(byte[] bytes, int start, int len)
        {
            if (start < 0)
                throw new ArgumentException("Start offset cannot be negative", "start");
            if (bytes.Length < (start + len))
                throw new ArgumentException("Byte array too small for requested offset and length");

            if (bytes.Length == len && start == 0)
            {
                NextBytes(bytes);
            }
            else
            {
#if !NETFX_CORE
				byte[] tmpBuf = new byte[len];
				rndProv.GetBytes(tmpBuf);
				Array.Copy(tmpBuf, 0, bytes, start, len);
#else
                IBuffer buf = CryptographicBuffer.GenerateRandom((uint)len);
                byte[] tmpBuf = new byte[buf.Length];
                CryptographicBuffer.CopyToByteArray(buf, out tmpBuf);
                Array.Copy(tmpBuf, 0, bytes, start, len);
#endif
            }
        }

        #endregion
    }
}

#endif
