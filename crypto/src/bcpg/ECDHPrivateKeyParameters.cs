﻿using System;

using Org.BouncyCastle.Asn1;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.Crypto.Parameters;

namespace Org.BouncyCastle.Bcpg
{
    /// <summary>
    /// Extended private key parameters for decryption ECDH private keys
    /// </summary>
    public class ECDHPrivateKeyParameters : ECPrivateKeyParameters
    {
        private byte keyWrapCipher;
        private byte kdfHash;
        private byte[] fingerprint;

        public ECDHPrivateKeyParameters(
            string algorithm,
            BigInteger d,
            DerObjectIdentifier publicKeyParamSet,
            byte cipher,
            byte hash,
            byte[] fingerprint)
            : base(algorithm, d, publicKeyParamSet)
        {
            this.keyWrapCipher = cipher;
            this.kdfHash = hash;
            this.fingerprint = fingerprint;
        }

        public byte[] Fingerprint
        {
            get { return fingerprint; }
        }

        public byte Hash
        {
            get { return kdfHash; }
        }

        public byte Cipher
        {
            get { return keyWrapCipher; }
        }
    }
}
