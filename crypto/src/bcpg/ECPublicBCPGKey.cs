﻿using System;
using System.Collections.Generic;
using System.Text;

using Org.BouncyCastle.Asn1;
using Org.BouncyCastle.Asn1.X9;
using Org.BouncyCastle.Asn1.Sec;
using Org.BouncyCastle.Asn1.Nist;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.Utilities;

namespace Org.BouncyCastle.Bcpg
{
    /// <summary>
    /// Base class for an OpenPGP Elliptic Curve public key
    /// </summary>
    public abstract class ECPublicBcpgKey
        : BcpgObject, IBcpgKey
    {
        /// <summary>
        /// NIST curve P-256
        /// </summary>
        private static readonly byte[] OID_1_2_840_10045_3_1_7 = { 0x2A, 0x86, 0x48, 0xCE, 0x3D, 0x03, 0x01, 0x07 };
        /// <summary>
        /// NIST curve P-384
        /// </summary>
        private static readonly byte[] OID_1_3_132_0_34 = { 0x2B, 0x81, 0x04, 0x00, 0x22 };
        /// <summary>
        /// NIST curve P-521
        /// </summary>
        private static readonly byte[] OID_1_3_132_0_35 = { 0x2B, 0x81, 0x04, 0x00, 0x23 };
        
        private MPInteger p;
        private DerObjectIdentifier curveOID;

        /// <summary>
        /// Prime curve ASN.1 OID object
        /// </summary>
        public DerObjectIdentifier CurveOid
        {
            get { return curveOID; }
            set { curveOID = value; }
        }

        /// <summary>
        /// Public ECC point
        /// </summary>
        public MPInteger P
        {
            get { return p; }
            set { p = value; }
        }

        public ECPublicBcpgKey(
            BigInteger p,
            DerObjectIdentifier curveOid)
        {
            this.p = new MPInteger(p);
            this.curveOID = curveOid;
        }

        /// <summary>
        /// Initializes an EC public key from an input stream
        /// </summary>
        /// <param name="bcpgIn">source stream</param>
        public ECPublicBcpgKey(
			BcpgInputStream bcpgIn)
		{
            int curveSize = bcpgIn.ReadByte();
            byte[] oidBytes = new byte[curveSize];
            bcpgIn.Read(oidBytes, 0, curveSize);
            
            if (Arrays.AreEqual(OID_1_2_840_10045_3_1_7, oidBytes))
            {
                curveOID = NistNamedCurves.GetOid("P-256");
            }
            else if (Arrays.AreEqual(OID_1_3_132_0_34, oidBytes))
            {
                curveOID = NistNamedCurves.GetOid("P-384");
            }
            else if (Arrays.AreEqual(OID_1_3_132_0_35, oidBytes))
            {
                curveOID = NistNamedCurves.GetOid("P-521");
            }
            else
            {
                throw new OpenPgp.PgpException("Unknown ECC Curve OID found in key definition!");
            }

            // ECC public point
            this.p = new MPInteger(bcpgIn);
		}

        internal Org.BouncyCastle.Math.EC.ECCurve Curve
        {
            get 
            {
                X9ECParameters curveParams = NistNamedCurves.GetByOid(curveOID);
                return curveParams.Curve; 
            }
        }

        public int BitLength
        {
            get { return Curve.FieldSize; }
        }

        /// <summary>The format, as a string, always "PGP".</summary>
        public string Format
        {
            get { return "PGP"; }
        }

        public override void Encode(
                    BcpgOutputStream bcpgOut)
        {
            byte[] curveOidEncoded = curveOID.GetDerEncoded();
            byte[] curveOidRfc6637Encoded = new byte[curveOidEncoded.Length - 2];
            Array.Copy(curveOidEncoded, 2, curveOidRfc6637Encoded, 0, curveOidRfc6637Encoded.Length);

            // size of the following field
            bcpgOut.WriteByte((byte)curveOidRfc6637Encoded.Length);
            // octets representing a curve OID
            bcpgOut.Write(curveOidRfc6637Encoded, 0, curveOidRfc6637Encoded.Length);
            // MPI of the public EC point
            bcpgOut.WriteObject(p);
        }
    }
}
