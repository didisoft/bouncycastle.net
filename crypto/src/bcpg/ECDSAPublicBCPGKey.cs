﻿using System;

using Org.BouncyCastle.Math;
using Org.BouncyCastle.Asn1;

namespace Org.BouncyCastle.Bcpg
{
    /// <summary>
    /// ECDSA OpenPGP public key
    /// </summary>
    public class ECDsaPublicBcpgKey
        : ECPublicBcpgKey
    {
        public ECDsaPublicBcpgKey(
			BcpgInputStream bcpgIn) : base(bcpgIn)
		{
		}

        public ECDsaPublicBcpgKey(
            BigInteger p,
            DerObjectIdentifier curveOid) 
            : base(p, curveOid)
        { }
    }
}
