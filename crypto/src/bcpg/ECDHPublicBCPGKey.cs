﻿using System;

using Org.BouncyCastle.Math;
using Org.BouncyCastle.Asn1;
using Org.BouncyCastle.Asn1.Nist;

namespace Org.BouncyCastle.Bcpg
{
    /// <summary>
    /// EC DH OpenPGP public key
    /// </summary>
    public class ECDHPublicBcpgKey : ECPublicBcpgKey
    {
        private byte hash;
        private byte cipher;

        /// <summary>
        /// Creates a EC DH public key following the security considerations from
        /// Rfc 6637, section 13. (Security Considerations)
        /// </summary>
        /// <param name="p"></param>
        /// <param name="curveOid"></param>
        public ECDHPublicBcpgKey (
            BigInteger p,
            DerObjectIdentifier curveOid) 
            : base(p, curveOid)
        {
            if (curveOid.Equals(NistNamedCurves.GetOid("P-256")))
            {
                this.hash = (byte)HashAlgorithmTag.Sha256;
                this.cipher = (byte)SymmetricKeyAlgorithmTag.Aes128;
            }
            else if (curveOid.Equals(NistNamedCurves.GetOid("P-384")))
            {
                this.hash = (byte)HashAlgorithmTag.Sha384;
                this.cipher = (byte)SymmetricKeyAlgorithmTag.Aes192;
            }
            else if (curveOid.Equals(NistNamedCurves.GetOid("P-521")))
            {
                this.hash = (byte)HashAlgorithmTag.Sha512;
                this.cipher = (byte)SymmetricKeyAlgorithmTag.Aes256;
            }
        }

        public ECDHPublicBcpgKey(BcpgInputStream bcpgIn)
            : base(bcpgIn)
        {
            int nextBytesCount = bcpgIn.ReadByte();

            // reserved byte, must be 0x1
            int reservedByte = bcpgIn.ReadByte();

            hash = (byte)bcpgIn.ReadByte();
            cipher = (byte)bcpgIn.ReadByte();

            // read if there are any extra bytes
            if (nextBytesCount > 3)
            {
                while (nextBytesCount > 3)
                {
                    bcpgIn.ReadByte();
                    --nextBytesCount;
                }
            }
        }

        public override void Encode(BcpgOutputStream bcpgOut)
        {
            base.Encode(bcpgOut);

            // size of the following fields
            bcpgOut.WriteByte(0x3);
            // value 01, reserved for future extensions
            bcpgOut.WriteByte(0x1);
            // hash function algorithm Id
            bcpgOut.WriteByte(hash);
            // symmetric key algorithm Id
            bcpgOut.WriteByte(cipher);
        }

        /// <summary>
        /// KDF (Key derivation function) hash algorithm Id
        /// </summary>
        public byte Hash
        {
            get { return hash; }
            set { hash = value; }
        }

        /// <summary>
        /// Symmetric KEK (Key encryption key) encryption algorithm Id
        /// </summary>
        public byte Cipher
        {
            get { return cipher; }
            set { cipher = value; }
        }
    }
}
