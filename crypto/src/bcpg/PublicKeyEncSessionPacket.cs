using System;
using System.IO;

using Org.BouncyCastle.Math;

namespace Org.BouncyCastle.Bcpg
{
	/// <remarks>Basic packet for a PGP public key.</remarks>
	public class PublicKeyEncSessionPacket
		: ContainedPacket //, PublicKeyAlgorithmTag
	{
		private int version;
		private long keyId;
		private PublicKeyAlgorithmTag algorithm;
		private BigInteger[] data;

        private byte[] eccPaddedSessionKey;

		internal PublicKeyEncSessionPacket(
			BcpgInputStream bcpgIn)
		{
			version = bcpgIn.ReadByte();

			keyId |= (long)bcpgIn.ReadByte() << 56;
			keyId |= (long)bcpgIn.ReadByte() << 48;
			keyId |= (long)bcpgIn.ReadByte() << 40;
			keyId |= (long)bcpgIn.ReadByte() << 32;
			keyId |= (long)bcpgIn.ReadByte() << 24;
			keyId |= (long)bcpgIn.ReadByte() << 16;
			keyId |= (long)bcpgIn.ReadByte() << 8;
			keyId |= (uint)bcpgIn.ReadByte();

			algorithm = (PublicKeyAlgorithmTag) bcpgIn.ReadByte();

			switch ((PublicKeyAlgorithmTag) algorithm)
			{
				case PublicKeyAlgorithmTag.RsaEncrypt:
				case PublicKeyAlgorithmTag.RsaGeneral:
					data = new BigInteger[]{ new MPInteger(bcpgIn).Value };
					break;
				case PublicKeyAlgorithmTag.ElGamalEncrypt:
				case PublicKeyAlgorithmTag.ElGamalGeneral:
					data = new BigInteger[]
					{
						new MPInteger(bcpgIn).Value,
						new MPInteger(bcpgIn).Value
					};
					break;
                case PublicKeyAlgorithmTag.EC:
                    MPInteger ephimeralKey = new MPInteger(bcpgIn);
                    data = new BigInteger[] { ephimeralKey.Value };

                    int eccPaddedSessionKeySize = bcpgIn.ReadByte();
                    if (eccPaddedSessionKeySize > 0xFF)
                        throw new IOException("EC symmetric key data is too long.");
                    this.eccPaddedSessionKey = new byte[eccPaddedSessionKeySize];
                    bcpgIn.Read(this.eccPaddedSessionKey, 0, eccPaddedSessionKeySize);
                    
                    break; 
				default:
					throw new IOException("unknown PGP public key algorithm encountered");
			}
		}

		public PublicKeyEncSessionPacket(
			long					keyId,
			PublicKeyAlgorithmTag	algorithm,
			BigInteger[]			data)
		{
			this.version = 3;
			this.keyId = keyId;
			this.algorithm = algorithm;
			this.data = (BigInteger[]) data.Clone();
		}

        public PublicKeyEncSessionPacket(
            long keyId,
            PublicKeyAlgorithmTag algorithm,
            BigInteger[] data,
            byte[] eccPaddedSessionKey)
            : this(keyId, algorithm, data)
        {
            this.eccPaddedSessionKey = eccPaddedSessionKey;
        }

		public int Version
		{
			get { return version; }
		}

		public long KeyId
		{
			get { return keyId; }
		}

		public PublicKeyAlgorithmTag Algorithm
		{
			get { return algorithm; }
		}

		public BigInteger[] GetEncSessionKey()
		{
			return (BigInteger[]) data.Clone();
		}

		public override void Encode(
			BcpgOutputStream bcpgOut)
		{
			MemoryStream bOut = new MemoryStream();
			BcpgOutputStream pOut = new BcpgOutputStream(bOut);

			pOut.WriteByte((byte) version);

			pOut.WriteLong(keyId);

			pOut.WriteByte((byte)algorithm);

            if (algorithm.Equals(PublicKeyAlgorithmTag.EC))
            {
                // encode the ephemeral point
                MPInteger.Encode(pOut, data[0]);

                // length of encrypted session key
                pOut.WriteByte((byte)this.eccPaddedSessionKey.Length);
                // encrypted session key
                pOut.Write(this.eccPaddedSessionKey);
            }
            else
            {
                for (int i = 0; i != data.Length; i++)
                {
                    MPInteger.Encode(pOut, data[i]);
                }
            }
			bcpgOut.WritePacket(PacketTag.PublicKeyEncryptedSession , bOut.ToArray(), true);
		}

        /// <summary>
        /// Additional padded session key as byte array. Used in ECDH encryption
        /// </summary>
        public byte[] EccPaddedSessionKey
        {
            get
            {
                return eccPaddedSessionKey;
            }
	    }
    }
}
