﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Org.BouncyCastle.Bcpg.Sig
{
    public class Placeholder : SignatureSubpacket
    {
        public Placeholder(
            bool    critical,
            byte[]     data)
            : base(SignatureSubpacketTag.Placeholder, critical, data)
        {}


        public bool IsAdk()
        {
            return (data.Length == 22) && (data[0] == 128) && (data[1] == 17);
        }

    }
}
