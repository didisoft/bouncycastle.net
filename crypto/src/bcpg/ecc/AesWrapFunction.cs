﻿using System;
using System.Collections.Generic;
using System.Text;

using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Bcpg.OpenPgp;

namespace Org.BouncyCastle.Bcpg.Ecc
{
    /// <summary>
    /// AES Wrap KEK function as described in RFC 6637
    /// </summary>
    public class AesWrapFunction : Rfc6637KeyDerivationFunction
    {
        // AES cipher
        private SymmetricKeyAlgorithmTag cipher;
        // data to be AES Wrapped
        private byte[] data;
        // KDF hash used as AES key
        private byte[] kdfHash;

        public AesWrapFunction(byte[] data)
        {
            this.data = data;
        }

        public void Init(SymmetricKeyAlgorithmTag cipher, byte[] kdfHash)
        {
            this.cipher = cipher;
            this.kdfHash = kdfHash;
        }

        public byte[] GenerateBytes()
        {
            // Prepare the AES KEK key
            byte[] aesKey = new byte[PgpUtilities.GetKeySize(this.cipher) / 8];
            Array.Copy(this.kdfHash, 0, aesKey, 0, aesKey.Length);

            // Add PKCS 5 padding to the session key
            byte[] sessionKey = new byte[(this.data.Length / 8)*8 + 8];
            Array.Copy(this.data, sessionKey, this.data.Length);
            Crypto.Paddings.Pkcs7Padding padding = new Crypto.Paddings.Pkcs7Padding();
            padding.AddPadding(sessionKey, this.data.Length);

            // AES Wrap the session key
            Crypto.Engines.AesWrapEngine aesEngine = new Crypto.Engines.AesWrapEngine();
            aesEngine.Init(true, new KeyParameter(aesKey));
            byte[] encKey = aesEngine.Wrap(sessionKey, 0, sessionKey.Length);
            return encKey;
        }
    }
}
