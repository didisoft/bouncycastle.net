﻿using System;
using System.Collections.Generic;
using System.Text;

using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Paddings;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Crypto.Engines;
using Org.BouncyCastle.Bcpg.OpenPgp;

namespace Org.BouncyCastle.Bcpg.Ecc
{
    /// <summary>
    /// AES Unwrap function as described in RFC 6637
    /// </summary>
    public class AesUnwrapFunction : Rfc6637KeyDerivationFunction
    {
        private SymmetricKeyAlgorithmTag cipher;
        private byte[] data;
        private byte[] kdfHash;

        public AesUnwrapFunction(byte[] data)
        {
            this.data = data;
        }

        public void Init(SymmetricKeyAlgorithmTag cipher, byte[] kdfHash)
        {
            this.cipher = cipher;            
            this.kdfHash = kdfHash;
        }

        public byte[] GenerateBytes()
        {
            int keySize = PgpUtilities.GetKeySize(this.cipher) / 8;

            // Unwrap the AES wrapped session key
            byte[] encryptedKey = this.data;
            AesWrapEngine aesEngine = new AesWrapEngine();
            aesEngine.Init(false, new KeyParameter(kdfHash, 0, keySize));
            byte[] unwrapped = aesEngine.Unwrap(encryptedKey, 0, encryptedKey.Length);

            // Return the key without the PKCS 5 padding
            int padCount = (new Pkcs7Padding()).PadCount(unwrapped);
            byte[] keyUnpadded = new byte[unwrapped.Length - padCount];
            Array.Copy(unwrapped, 0, keyUnpadded, 0, unwrapped.Length - padCount);
            return keyUnpadded;
        }
    }
}
