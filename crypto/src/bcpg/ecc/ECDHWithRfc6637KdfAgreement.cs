﻿using System;
using System.Collections;
using System.IO;

using Org.BouncyCastle.Asn1;
using Org.BouncyCastle.Asn1.Nist;
using Org.BouncyCastle.Asn1.Pkcs;
using Org.BouncyCastle.Asn1.X9;
using Org.BouncyCastle.Crypto;
using Org.BouncyCastle.Crypto.Agreement;
using Org.BouncyCastle.Crypto.Parameters;
using Org.BouncyCastle.Math;
using Org.BouncyCastle.Security;

using Org.BouncyCastle.Bcpg;
using Org.BouncyCastle.Bcpg.OpenPgp;

using Org.BouncyCastle.Math.EC;

namespace Org.BouncyCastle.Bcpg.Ecc
{
    /// <summary>
    /// ECDH key derivation function as described in 
    /// RFC 6637 (Section 7 and 8)
    /// </summary>
    public class ECDHWithRfc6637KdfAgreement
    {
        // 20 bytes from the string "Anonymous Sender    " UTF-8 encoded
        private static readonly byte[] anonymousSenderConstant = { 0x41, 0x6E, 0x6F, 0x6E, 0x79, 0x6D, 0x6F, 0x75, 0x73, 0x20, 0x53, 0x65, 0x6E, 0x64, 0x65, 0x72, 0x20, 0x20, 0x20, 0x20 };
        private Rfc6637KeyDerivationFunction kdf;
        private byte cipher; 
        private byte hash;
        private byte[] data;
        private byte[] fingerprint;
        private byte[] ephemeralPoint;
        private ECPrivateKeyParameters privKey;
        private ECPublicKeyParameters pubKey;

        public byte[] EphemeralPoint
        {
            get { return ephemeralPoint; }
            set { ephemeralPoint = value; }
        }

        public ECDHWithRfc6637KdfAgreement(byte[] data, byte cipher, byte hash, byte[] fingerprint)
		{
            this.data = data;
            this.cipher = cipher; 
            this.hash = hash;
            this.fingerprint = fingerprint;
		}

        public string AlgorithmName 
        {
            get
            {
                return "ECDH";
            }
        }

        public void Init(bool forWrapping, ICipherParameters parameters)
        {            
            if (forWrapping) 
            {
                // Generate ephemeral point
                SecureRandom random;
                if (parameters is ParametersWithRandom) 
                {
                    random = ((ParametersWithRandom)parameters).Random;
                    pubKey = (ECPublicKeyParameters)((ParametersWithRandom)parameters).Parameters;
                } else 
                {
                    random = new SecureRandom();
                    pubKey = (ECPublicKeyParameters)parameters;
                }
                ECKeyGenerationParameters ecKeyParams = new ECKeyGenerationParameters(pubKey.PublicKeyParamSet, random);
                IAsymmetricCipherKeyPairGenerator ecKeyGen = GeneratorUtilities.GetKeyPairGenerator("ECDH");
                ecKeyGen.Init(ecKeyParams);
                AsymmetricCipherKeyPair ephemeralKeyPair = ecKeyGen.GenerateKeyPair();
                ECPublicKeyParameters ephemeralPubKey = (ECPublicKeyParameters)ephemeralKeyPair.Public;

                this.privKey = (ECPrivateKeyParameters)ephemeralKeyPair.Private;
                this.ephemeralPoint = ephemeralPubKey.Q.GetEncoded();

                this.kdf = new AesWrapFunction(this.data);
            } else {
                privKey = (ECDHPrivateKeyParameters)parameters;

                // Ephemeral key loading
                FpCurve curve = (FpCurve)privKey.Parameters.Curve;
                FpPoint ephemeralPoint = (FpPoint)curve.DecodePoint(this.ephemeralPoint);

                // Elliptic Curve Secret Value Derivation Primitive, Diffie-Hellman version
                pubKey = new ECPublicKeyParameters("ECDH", ephemeralPoint, privKey.PublicKeyParamSet);

                this.kdf = new AesUnwrapFunction(this.data);
            }
        }

        private byte GetPubKeyAlgorithmId(string algorithmName)
        {
            switch (algorithmName)
            {
                case "EC": return (byte)PublicKeyAlgorithmTag.EC;
                case "ECDH": return (byte)PublicKeyAlgorithmTag.EC;
                case "ECDSA": return (byte)PublicKeyAlgorithmTag.ECDsa;
                default: throw new PgpException("Invalid algorithm name " + algorithmName);
            }
        }

		public byte[] CalculateAgreement()
		{
            // Elliptic Curve Secret Value Derivation Primitive, Diffie-Hellman version
            //ECPublicKeyParameters pubKey = new ECPublicKeyParameters("ECDH", ephemeralPoint, privKeyParams.PublicKeyParamSet);
            Crypto.Agreement.ECDHBasicAgreement agreement = new Org.BouncyCastle.Crypto.Agreement.ECDHBasicAgreement();
            agreement.Init(privKey);            
            byte[] sharedX = agreement.CalculateAgreement(pubKey).ToByteArrayUnsigned();            

            // Load the curve Oid bytes with the first two bytes removed
            byte[] curveOidEncoded = privKey.PublicKeyParamSet.GetDerEncoded();
            byte[] curveOidRfc6637Encoded = new byte[curveOidEncoded.Length - 2];
            Array.Copy(curveOidEncoded, 2, curveOidRfc6637Encoded, 0, curveOidRfc6637Encoded.Length);

            // KDF parameters initialization
            MemoryStream kdfParam = new MemoryStream();
            kdfParam.WriteByte((byte)curveOidRfc6637Encoded.Length);
            kdfParam.Write(curveOidRfc6637Encoded, 0, curveOidRfc6637Encoded.Length);
            kdfParam.WriteByte(GetPubKeyAlgorithmId(pubKey.AlgorithmName));
            // next bytes length
            kdfParam.WriteByte(0x3);
            // reserved
            kdfParam.WriteByte(0x1);
            kdfParam.WriteByte(this.hash);
            kdfParam.WriteByte(this.cipher);
            // 20 bytes from the string "Anonymous Sender    " UTF-8 encoded
            kdfParam.Write(anonymousSenderConstant, 0, anonymousSenderConstant.Length);
            // 20 bytes from the key Fingerprint 
            kdfParam.Write(this.fingerprint, 0, 20);

            // KDF Hash input = 0 | 0 | 0 | 1 | shared point x | KDF parameters
            MemoryStream MB = new MemoryStream();
            MB.WriteByte(0);
            MB.WriteByte(0);
            MB.WriteByte(0);
            MB.WriteByte(1);
            MB.Write(sharedX, 0, sharedX.Length);
            MB.Write(kdfParam.ToArray(), 0, (int)kdfParam.Position);
            HashAlgorithmTag hash = (HashAlgorithmTag)this.hash;
            byte[] kdfHash = DigestUtilities.CalculateDigest(PgpUtilities.GetDigestName(hash),
                                                            MB.ToArray());


            kdf.Init((SymmetricKeyAlgorithmTag)this.cipher, kdfHash);

            return kdf.GenerateBytes();
        }
    }
}
