﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Org.BouncyCastle.Bcpg.Ecc
{
    /// <summary>
    /// Base interface for RFC6637 AES Wrap and Unwrap KEK functions
    /// </summary>
    public interface Rfc6637KeyDerivationFunction
    {
        void Init(SymmetricKeyAlgorithmTag cipher, byte[] kdfHash);

        byte[] GenerateBytes();
    }
}
