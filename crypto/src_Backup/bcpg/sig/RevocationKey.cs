﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Org.BouncyCastle.Bcpg
{
    /// <summary>
    /// Represents revocation key OpenPGP signature sub packet.
    /// </summary>
    public class RevocationKey : SignatureSubpacket
    {
        public const byte CLASS_DEFAULT = (byte)0x80;

        public const byte CLASS_SENSITIVE = (byte)0x40;

        // 1 octet of class, 
        // 1 octet of public-key algorithm ID, 
        // 20 octets of fingerprint

        public RevocationKey(bool isCritical, byte[] data)
            : base(SignatureSubpacketTag.RevocationKey, isCritical, data)
        {
        }

        public RevocationKey(bool isCritical,
                byte signatureClass,
                byte keyAlgorithm,
                byte[] fingerprint)
            : base(SignatureSubpacketTag.RevocationKey, isCritical, CreateData(signatureClass, keyAlgorithm, fingerprint))
        {
        }

        private static byte[] CreateData(byte signatureClass, byte keyAlgorithm, byte[] fingerprint)
        {
            byte[] data = new byte[2 + fingerprint.Length];

            data[0] = signatureClass;
            data[1] = keyAlgorithm;
            System.Array.Copy(fingerprint, 0, data, 2, fingerprint.Length);

            return data;
        }

        public byte SignatureClass
        {
            get
            {
                return this.GetData()[0];
            }
        }

        public byte Algorithm
        {
            get
            {
                return this.GetData()[1];
            }
        }

        public byte[] GetFingerprint()
        {
            byte[] data = this.GetData();

            byte[] fingerprint = new byte[data.Length - 2];

            System.Array.Copy(data, 2, fingerprint, 0, fingerprint.Length);

            return fingerprint;
        }
    }

}
